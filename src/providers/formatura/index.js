import { createContext, useState } from "react";

export const FormaturaContext = createContext();

export const FormaturaProvider = ({ children }) => {
  const [listaFormatura, setListaFormatura] = useState([]);

  const addPraFormatura = (item) => {
    setListaFormatura([...listaFormatura, item]);
  };

  const removerDaFormatura = (item) => {
    const newListaFormatura = listaFormatura.filter(
      (bebida) => bebida.id !== item.id
    );
    setListaFormatura(newListaFormatura);
  };

  return (
    <FormaturaContext.Provider
      value={{ listaFormatura, addPraFormatura, removerDaFormatura }}
    >
      {children}
    </FormaturaContext.Provider>
  );
};
