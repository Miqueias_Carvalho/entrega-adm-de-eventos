import { CatalogoProvider } from "./catalogo";
import { CasamentoProvider } from "./casamento";
import { ConfraterniozacaoProvider } from "./confraternizacao";
import { FormaturaProvider } from "./formatura";

const Providers = ({ children }) => {
  return (
    <CatalogoProvider>
      <CasamentoProvider>
        <ConfraterniozacaoProvider>
          <FormaturaProvider>{children}</FormaturaProvider>
        </ConfraterniozacaoProvider>
      </CasamentoProvider>
    </CatalogoProvider>
  );
};
export default Providers;
