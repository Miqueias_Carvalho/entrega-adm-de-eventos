import { createContext, useState } from "react";

export const CasamentoContext = createContext();

export const CasamentoProvider = ({ children }) => {
  const [listaCasamento, setListaCasamento] = useState([]);

  const addProCasamento = (item) => {
    setListaCasamento([...listaCasamento, item]);
  };

  const removerDoCasamento = (item) => {
    const newListaCasamento = listaCasamento.filter(
      (bebida) => bebida.id !== item.id
    );
    setListaCasamento(newListaCasamento);
  };

  return (
    <CasamentoContext.Provider
      value={{ listaCasamento, addProCasamento, removerDoCasamento }}
    >
      {children}
    </CasamentoContext.Provider>
  );
};
