import { createContext, useState, useEffect } from "react";
import api from "../../services/api";

export const CatalogoContext = createContext();

export const CatalogoProvider = ({ children }) => {
  const [catalogo, setCatalogo] = useState([]);
  const [page, setPage] = useState(1);

  const loadDrinks = () => {
    api
      .get(`/beers?page=${page}`)
      .then((response) => {
        setCatalogo([...catalogo, ...response.data]);
      })
      .catch((err) => console.log(err.response))
      .then(() => {
        if (page < 13) {
          setPage(page + 1);
        }
      });
  };

  useEffect(() => {
    loadDrinks();
  }, [page]);

  const addProCatalogo = (item) => {
    setCatalogo([...catalogo, item]);
  };

  const removerDoCatalogo = (item) => {
    const newCatalogo = catalogo.filter((bebida) => bebida.id !== item.id);
    setCatalogo(newCatalogo);
  };

  return (
    <CatalogoContext.Provider
      value={{ catalogo, addProCatalogo, removerDoCatalogo }}
    >
      {children}
    </CatalogoContext.Provider>
  );
};
