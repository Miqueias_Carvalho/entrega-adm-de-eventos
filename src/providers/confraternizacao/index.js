import { createContext, useState } from "react";

export const ConfraternizacaoContext = createContext();

export const ConfraterniozacaoProvider = ({ children }) => {
  const [listaConfraternizacao, setListaConfraternizacao] = useState([]);

  const addPraConfraternizacao = (item) => {
    setListaConfraternizacao([...listaConfraternizacao, item]);
  };

  const removerDaConfraternizacao = (item) => {
    const newListaConfraternizacao = listaConfraternizacao.filter(
      (bebida) => bebida.id !== item.id
    );
    setListaConfraternizacao(newListaConfraternizacao);
  };

  return (
    <ConfraternizacaoContext.Provider
      value={{
        listaConfraternizacao,
        addPraConfraternizacao,
        removerDaConfraternizacao,
      }}
    >
      {children}
    </ConfraternizacaoContext.Provider>
  );
};
