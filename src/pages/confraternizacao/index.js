import { useContext } from "react";
import { ConfraternizacaoContext } from "../../providers/confraternizacao";
import Bebidas from "../../components/Bebidas";

function Confraternizacao() {
  const { listaConfraternizacao } = useContext(ConfraternizacaoContext);

  return (
    <div>
      <h2>Confraternização</h2>
      <Bebidas type="confraternizacao" lista={listaConfraternizacao} />
    </div>
  );
}
export default Confraternizacao;
