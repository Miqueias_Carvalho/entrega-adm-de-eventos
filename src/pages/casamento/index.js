import { useContext } from "react";
import { CasamentoContext } from "../../providers/casamento";
import Bebidas from "../../components/Bebidas";

function Casamento() {
  const { listaCasamento } = useContext(CasamentoContext);

  return (
    <div>
      <h2>Casamento</h2>
      <Bebidas type="casamento" lista={listaCasamento} />
    </div>
  );
}
export default Casamento;
