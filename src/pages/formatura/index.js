import { useContext } from "react";
import { FormaturaContext } from "../../providers/formatura";
import Bebidas from "../../components/Bebidas";

function Formatura() {
  const { listaFormatura } = useContext(FormaturaContext);

  return (
    <div>
      <h2>Formatura</h2>
      <Bebidas type="formatura" lista={listaFormatura} />
    </div>
  );
}
export default Formatura;
