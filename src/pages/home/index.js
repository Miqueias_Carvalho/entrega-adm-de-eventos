import { useContext } from "react";
import { CatalogoContext } from "../../providers/catalogo";
import Bebidas from "../../components/Bebidas";

function Home() {
  const { catalogo } = useContext(CatalogoContext);

  return (
    <div>
      <h2>Catalogo de Bebidas</h2>
      <Bebidas lista={catalogo} />
    </div>
  );
}
export default Home;
