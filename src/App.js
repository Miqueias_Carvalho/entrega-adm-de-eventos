import GlobalStyle from "./styles/global";
import ToolBar from "./components/Toolbar";
import Routes from "./routes";

function App() {
  return (
    <>
      <GlobalStyle />
      <ToolBar />
      <Routes />
    </>
  );
}

export default App;
