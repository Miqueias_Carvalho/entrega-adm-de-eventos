import styled from "styled-components";

export const Container = styled.nav`
  width: 100%;
  height: 150px;
  padding: 10px 0;
  background-color: lightblue;
  margin-bottom: 24px;
  text-align: center;
  display: flex;

  align-items: center;
  justify-content: center;

  ul {
    list-style: none;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    width: 100%;
    max-width: 350px;
    height: 100px;
  }

  li {
    a {
      color: #551a8b;
    }
  }

  @media (min-width: 340px) {
    height: 50px;

    ul {
      height: 100%;
      flex-direction: row;
      align-items: center;

      li {
        height: 100%;
        padding: 5px 0;

        a {
          color: #551a8b;
          font-size: 18px;
        }
        &:hover {
          background-color: #0a34586b;
        }
      }
    }
  }
`;
