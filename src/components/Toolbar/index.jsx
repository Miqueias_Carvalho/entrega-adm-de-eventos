import { Container } from "./styles";
import { Link } from "react-router-dom";

function ToolBar() {
  return (
    <Container>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/casamento">Casamento</Link>
        </li>
        <li>
          <Link to="/formatura">Formatura</Link>
        </li>
        <li>
          <Link to="/confraternizacao">Confraternização</Link>
        </li>
      </ul>
    </Container>
  );
}

export default ToolBar;
