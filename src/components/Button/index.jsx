import { Container, ButtonAdd, ButtonRemover } from "./styles";
import { useContext } from "react";
import { CasamentoContext } from "../../providers/casamento";
import { FormaturaContext } from "../../providers/formatura";
import { ConfraternizacaoContext } from "../../providers/confraternizacao";
import { CatalogoContext } from "../../providers/catalogo";

function Button({ item, type }) {
  const { addProCatalogo, removerDoCatalogo } = useContext(CatalogoContext);
  const { addProCasamento, removerDoCasamento } = useContext(CasamentoContext);
  const { addPraFormatura, removerDaFormatura } = useContext(FormaturaContext);
  const { addPraConfraternizacao, removerDaConfraternizacao } = useContext(
    ConfraternizacaoContext
  );

  const removerItem = () => {
    if (type === "casamento") {
      removerDoCasamento(item);
    } else if (type === "formatura") {
      removerDaFormatura(item);
    } else if (type === "confraternizacao") {
      removerDaConfraternizacao(item);
    }

    addProCatalogo(item);
  };

  const addCasamento = () => {
    addProCasamento(item);
    removerDoCatalogo(item);
  };

  const addFormatura = () => {
    addPraFormatura(item);
    removerDoCatalogo(item);
  };

  const addConfraternizacao = () => {
    addPraConfraternizacao(item);
    removerDoCatalogo(item);
  };

  return (
    <Container>
      {type === "todos" && (
        <>
          <ButtonAdd onClick={addCasamento}>Add para o casamento</ButtonAdd>
          <ButtonAdd onClick={addFormatura}>Add para a formatura</ButtonAdd>
          <ButtonAdd onClick={addConfraternizacao}>
            Add para o confraternização
          </ButtonAdd>
        </>
      )}

      {type === "casamento" && (
        <>
          <ButtonRemover onClick={removerItem}>Remover</ButtonRemover>
        </>
      )}

      {type === "formatura" && (
        <>
          <ButtonRemover onClick={removerItem}>Remover</ButtonRemover>
        </>
      )}

      {type === "confraternizacao" && (
        <>
          <ButtonRemover onClick={removerItem}>Remover</ButtonRemover>
        </>
      )}
    </Container>
  );
}
export default Button;
