import styled from "styled-components";

export const Container = styled.div`
  margin: 16px 0;

  display: flex;
  flex-direction: column;
  align-items: stretch;

  button {
    border-radius: 5px;
    background-color: lightblue;
    padding: 8px;
    font-size: 16px;
    color: #1622b3;
    font-weight: 600;
  }

  @media (min-width: 635px) {
    flex-direction: row;
    justify-content: flex-start;

    button + button {
      margin-left: 5px;
    }
  }
`;

export const ButtonAdd = styled.button``;

export const ButtonRemover = styled.button`
  @media (min-width: 635px) {
    width: 200px;
  }
`;
