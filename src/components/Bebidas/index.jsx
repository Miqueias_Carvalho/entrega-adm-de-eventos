import { Container, CardContainer, Imagem, Info, Bebida } from "./styles";
import notFound from "../../assets/204330.png";
import Button from "../Button";

function Bebidas({ lista, type = "todos" }) {
  return (
    <Container>
      {lista.map((drink, index) => (
        <CardContainer key={index}>
          <Bebida>
            <Imagem>
              <img
                src={!!drink.image_url ? drink.image_url : notFound}
                alt="imagem"
              />
            </Imagem>
            <Info>
              <h3>{drink.name}</h3>
              <p>
                <span>{drink.first_brewed}</span>
              </p>
              <p>{drink.description}</p>
              <p>{drink.volume.value} litros</p>
              {/* <p>{drink.id}</p> */}
            </Info>
          </Bebida>

          <Button item={drink} type={type} />
        </CardContainer>
      ))}
    </Container>
  );
}
export default Bebidas;
