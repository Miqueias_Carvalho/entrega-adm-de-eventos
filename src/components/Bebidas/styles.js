import styled from "styled-components";

export const Container = styled.div`
  width: 95%;
  margin: 32px auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
`;

export const CardContainer = styled.div`
  width: 100%;
`;

export const Bebida = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  @media (min-width: 450px) {
    flex-direction: row;
  }
`;

export const Imagem = styled.div`
  width: 120px;
  /* height: 100px; */

  img {
    width: 120px;
    height: 150px;
    object-fit: scale-down;
  }
`;

export const Info = styled.div`
  p {
    width: 96%;

    span {
      font-style: italic;
      color: gray;
      line-height: 2;
    }
  }
`;
