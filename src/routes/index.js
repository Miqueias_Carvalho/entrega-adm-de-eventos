import { Switch, Route } from "react-router-dom";
import Home from "../pages/home";
import Casamento from "../pages/casamento";
import Formatura from "../pages/formatura";
import Confraternizacao from "../pages/confraternizacao";

function Routes() {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/formatura">
        <Formatura />
      </Route>
      <Route path="/casamento">
        <Casamento />
      </Route>
      <Route path="/confraternizacao">
        <Confraternizacao />
      </Route>
    </Switch>
  );
}

export default Routes;
